/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class EscrituraClass {

    private String codigo;
    private String tipoEscritura;
    private String nombreFichero;
    private int numeroIntervinientes;

    
    /**
     * Cosntructor de la clase EscrituraClass solo con el codigo de escritura
     * @param codigo String
     */
    public EscrituraClass(String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Constructor de la clase EscrituraClass completo
     * @param codigo String
     * @param tipoEscritura String
     * @param nombreFichero String
     * @param numeroIntervinientes Integer 
     */
    public EscrituraClass(String codigo, String tipoEscritura, String nombreFichero, int numeroIntervinientes) {
        this.codigo = codigo;
        this.tipoEscritura = tipoEscritura;
        this.nombreFichero = nombreFichero;
        this.numeroIntervinientes = numeroIntervinientes;
    }

    public int getNumeroIntervinientes() {
        return numeroIntervinientes;
    }

    public void setNumeroIntervinientes(int numeroIntervinientes) {
        this.numeroIntervinientes = numeroIntervinientes;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipoEscritura() {
        return tipoEscritura;
    }

    public void setTipoEscritura(String tipoEscritura) {
        this.tipoEscritura = tipoEscritura;
    }

    public String getNombreFichero() {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }
    
}
