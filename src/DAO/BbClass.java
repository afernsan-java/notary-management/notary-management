/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public final class BbClass {

    Connection conn;
    Statement sentencia;
    ResultSet resul;
    ClienteClass cliente;
    EscrituraClass escritura;

    public BbClass() throws SQLException {

    }

    /**
     * Conexion general con BBDD<br>
     *
     * Metodo para la conexion de java con la base de datos MySql
     *
     * @throws SQLException Excepcion de tipo SQL
     */
    public void conexion() throws SQLException {
        // Establecemos la conn con la BD
        //Primer campo "biblioteca" Nombre de la BD, Segundo campo "root" Usuario,Tercer campo Clave "".
        try {
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/sys", "root", "");
        } catch (SQLException e) {
            System.out.println("sqalto la liebre");
        }
    }

    /**
     * Metodo que crea una base datos
     *
     * @return Boolean
     */
    public boolean creacionBBDD() {
        String url = "jdbc:mysql://localhost";
        String username = "root";
        String password = "";
        String sql = "CREATE DATABASE IF NOT EXISTS notaria";
        try (Connection conexion = DriverManager.getConnection(url, username, password);
                PreparedStatement stmt = conexion.prepareStatement(sql)) {
            stmt.execute();
            return true;
        } catch (Exception e) {
            System.out.println("error en la creacion " + e);
            return false;
        }
    }

    /**
     * Metodo para saber si la base de datos 'notaria' existe
     *
     * @return Boolean
     */
    public boolean dbExists() {
        try {
            String db = "notaria";
            String pass = "";
            conn = null;
            sentencia = null;
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/sys", "root", "");
            sentencia = conn.createStatement();
            String sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + db + "'";
            ResultSet rs = sentencia.executeQuery(sql);
            if (rs.next()) {
                //JOptionPane.showMessageDialog(main, "La base de datos existe.");                
                return true;
            }
        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(main, "La base de datos no existe.");
            return false;
        }
        return false;
    }

    /**
     * Metodo que me dira si una tablas esta creada
     *
     * @param nombreTabla String
     * @return Boolean Boolean
     */
    public boolean tablasExists(String nombreTabla) {
        try {
            conn = null;
            sentencia = null;
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
            sentencia = conn.createStatement();
            String sql = "Select * from " + nombreTabla;
            ResultSet rs = sentencia.executeQuery(sql);

            if (rs.next()) {
                //JOptionPane.showMessageDialog(main, "La base de datos existe.");   
                return true;

            }
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            return false;
        }
    }

    /**
     * Metodo que me dira si una tabla tiene algun registro creado o no
     *
     * @param nombreTabla String
     * @return Boolean Boolean
     */
    public boolean getTablaCountInit(String nombreTabla) {
        try {
            conn = null;
            sentencia = null;
            int contador = 0;
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
            sentencia = conn.createStatement();
            String sql = "Select * from " + nombreTabla;
            ResultSet rs = sentencia.executeQuery(sql);

            while (rs.next()) {
                //JOptionPane.showMessageDialog(main, "La base de datos existe.");   
                contador = rs.getRow();
            }
            if (contador > 0) {
                return true;
            }
        } catch (ClassNotFoundException | SQLException e) {

        }
        return false;
    }

    /**
     * Metodo para crear las tablas para la database notaria
     *
     * @param nombreTabla String
     */
    public void creacionTabla(String nombreTabla) {
        try {
            String url = "jdbc:mysql://localhost/notaria";
            String username = "root";
            String password = "";
            Connection conexion = DriverManager.getConnection(url, username, password);
            PreparedStatement stmt = null;
            switch (nombreTabla) {
                case "Clientes":
                    stmt = conexion.prepareStatement("CREATE TABLE " + nombreTabla + " (ID int NOT NULL AUTO_INCREMENT, Cod_Cliente varchar(50) NOT NULL UNIQUE, Nombre varchar(30), telefono INT(20), PRIMARY KEY (ID))");
                    break;
                case "Escrituras":
                    stmt = conexion.prepareStatement("CREATE TABLE " + nombreTabla + " (ID int NOT NULL AUTO_INCREMENT, Codigo varchar(50) NOT NULL UNIQUE, Tipo varchar(30), Nom_fic varchar(150) UNIQUE, Num_interv INT(3), PRIMARY KEY (ID))");
                    break;
                case "EscCli":
                    stmt = conexion.prepareStatement("CREATE TABLE " + nombreTabla + " (ID int NOT NULL AUTO_INCREMENT, codCli varchar(50) NOT NULL, codEsc varchar(50) NOT NULL, PRIMARY KEY (ID))");
                    break;
                default:
                    break;
            }
            stmt.execute();
            stmt.close();
        } catch (SQLException sqle) {
            System.out.println("Error en la ejecución: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());
        }
    }

    /**
     * Metodo que busca la existencia de un cliente
     *
     * @param clienteCodigo Object - Objeto de la clase ClienteClass
     * @return Boolean
     * @throws SQLException Excepcion
     */
    public boolean isCliente(ClienteClass clienteCodigo) throws SQLException {
        String codigoCliente = clienteCodigo.getCodigo();
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        int existeCliente = 0;//control para la existencia de un cliente     
        sentencia = (com.mysql.jdbc.Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM clientes WHERE Cod_Cliente = '" + codigoCliente + "'  ");
        while (resul.next()) {
            existeCliente = resul.getRow();
        }
        if (existeCliente == 1) {
            return true;
        }
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        conn.close();
        return false;
    }

    /**
     * Metodo que inserta un nuevo cliente en su tabla correspondiente
     *
     * @param cliente Object - Objeto de la clase ClienteClass
     * @throws SQLException Excepcion
     */
    public void insertCliente(ClienteClass cliente) throws SQLException {
        String Cod_Cliente = cliente.getCodigo();
        String nombre = cliente.getNombre();
        int telefono = cliente.getTelefono();
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        // the mysql insert statement
        String query = " INSERT INTO clientes (Cod_Cliente,nombre,telefono)"
                + " values (?, ?, ?)";
        try ( // create the mysql insert preparedstatement
                com.mysql.jdbc.PreparedStatement preparedStmt = (com.mysql.jdbc.PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setString(1, Cod_Cliente);
            preparedStmt.setString(2, nombre);
            preparedStmt.setInt(3, telefono);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        conn.close();
    }

    /**
     * Metodo que devolvera un objeto de la clase ClienteClass
     *
     * @param codCliente String - codigo del cliente
     * @return Object - Objeto de la clase ClienteClass
     * @throws SQLException Excepcion
     */
    public ClienteClass getObjectCliente(String codCliente) throws SQLException {
        String codCli = "", nombre = "";
        Integer telefono = 0;
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        sentencia = (com.mysql.jdbc.Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM clientes WHERE Cod_Cliente = '" + codCliente + "'  ");
        while (resul.next()) {
            codCli = resul.getString(2);
            nombre = resul.getString(3);
            telefono = resul.getInt(4);
        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return new ClienteClass(codCli, nombre, telefono);
    }

    /**
     * Metodo para actualizar o modificar los registros de la tabla clientes
     *
     * @param clienteCodigo Object - Objeto de la clase ClienteClass
     * @throws SQLException Excepcion
     */
    public void updateCliente(ClienteClass clienteCodigo) throws SQLException {
        cliente = clienteCodigo;
        String codCliente = clienteCodigo.getCodigo();
        String nombre = clienteCodigo.getNombre();
        int telefono = clienteCodigo.getTelefono();
        // the mysql insert statement
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        String query = "UPDATE clientes SET nombre=? ,telefono=?  WHERE Cod_Cliente= '" + codCliente + "'  ";
        System.out.println(query);
        try ( // create the mysql insert preparedstatement
                com.mysql.jdbc.PreparedStatement preparedStmt = (com.mysql.jdbc.PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setString(1, nombre);
            preparedStmt.setInt(2, telefono);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

    /**
     * Metodo que busca la existencia de una escritura
     *
     * @param escritura Object - Objeto de la clase EscrituraClass
     * @return Boolean
     * @throws SQLException Excepcion
     */
    public boolean isEscritura(EscrituraClass escritura) throws SQLException {
        String codigoEscritura = escritura.getCodigo();
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        int existeEscritura = 0;//control para la existencia de un cliente     
        sentencia = (com.mysql.jdbc.Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM escrituras WHERE Codigo = '" + codigoEscritura + "'  ");
        while (resul.next()) {
            existeEscritura = resul.getRow();
        }
        if (existeEscritura == 1) {
            return true;
        }
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        conn.close();
        return false;
    }

    /**
     * Metodo que inserta una nueva escritura en su tabla correspondiente
     *
     * @param escritura Object - Objeto de la clase EscrituraClass
     * @throws SQLException Excepcion
     */
    public void insertEscritura(EscrituraClass escritura) throws SQLException {

        String codigoEscritura = escritura.getCodigo();
        String tipoEscritura = escritura.getTipoEscritura();
        String nombreFicheroEscritura = escritura.getNombreFichero();
        int numeroIntervinientes = escritura.getNumeroIntervinientes();

        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        // the mysql insert statement
        String query = " INSERT INTO escrituras (Codigo,Tipo,Nom_fic, Num_interv)"
                + " values (?, ?, ?, ?)";
        try ( // create the mysql insert preparedstatement
                com.mysql.jdbc.PreparedStatement preparedStmt = (com.mysql.jdbc.PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setString(1, codigoEscritura);
            preparedStmt.setString(2, tipoEscritura);
            preparedStmt.setString(3, nombreFicheroEscritura);
            preparedStmt.setInt(4, numeroIntervinientes);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        conn.close();
    }

    /**
     * Metodo que devolvera un objeto de la clase EscrituraClass
     *
     * @param codigoEscritura String - Codigo de la escritura
     * @return Object - Objeto de la clase EscrituraClass
     * @throws SQLException Excepcion
     */
    public EscrituraClass getObjectEscritura(String codigoEscritura) throws SQLException {
        String nombreFichero = "", tipo = "";
        Integer numInterv = 0;
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        sentencia = (com.mysql.jdbc.Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM escrituras WHERE Codigo = '" + codigoEscritura + "'  ");
        while (resul.next()) {
            codigoEscritura = resul.getString(2);
            tipo = resul.getString(3);
            nombreFichero = resul.getString(4);
            numInterv = resul.getInt(5);
        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return new EscrituraClass(codigoEscritura, tipo, nombreFichero, numInterv);
    }

    /**
     * Metodo que inserta un nuevo registro en EscCli
     *
     * @param codCliente String - Codigo de cliente
     * @param codEscritura String - Codigo de Escritura
     * @throws SQLException Excepcion
     */
    public void insertEscCli(String codCliente, String codEscritura) throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        // the mysql insert statement
        String query = " INSERT INTO esccli (CodCli,CodEsc)"
                + " values (?, ?)";
        try ( // create the mysql insert preparedstatement
                com.mysql.jdbc.PreparedStatement preparedStmt = (com.mysql.jdbc.PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setString(1, codCliente);
            preparedStmt.setString(2, codEscritura);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        conn.close();
    }

    /**
     * Metodo que devolvera el codigo de cliente asociado al codigo de escritura
     * en la tabla esccli
     *
     * @param codEscritura String - Codigo de escritura
     * @return String - Codigo de cliente
     * @throws SQLException Excepcion
     */
    public String getEscliCodigoCliente(String codEscritura) throws SQLException {
        String codCliente = "";
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        sentencia = (com.mysql.jdbc.Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM esccli WHERE codEsc = '" + codEscritura + "'  ");

        while (resul.next()) {
            codCliente = resul.getString(2);
        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return codCliente;
    }

    /**
     * Metodo que devuelve un arrayList de Objetosd de la clase ClientesClass
     *
     * @return ArraList de objetos clientes
     * @throws SQLException Excepcion
     */
    public ArrayList getArrayListCLientes() throws SQLException {
        ArrayList<ClienteClass> arrayClientesLista = new ArrayList<>();
        String codCli = "", nombre = "";
        Integer telefono = 0;
        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        sentencia = (com.mysql.jdbc.Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM clientes");
        while (resul.next()) {
            codCli = resul.getString(2);
            nombre = resul.getString(3);
            telefono = resul.getInt(4);
            cliente = new ClienteClass(resul.getString(2), resul.getString(3), resul.getInt(4));
            arrayClientesLista.add(cliente);
        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement        
        return arrayClientesLista;
    }

    /**
     * Metodo que devolvera un ArrayList con las escrituras con el tipo de
     * escrito que se indique
     *
     * @param tipoEscritura String - Tipo de escritura
     * @return ArrayList - array de objetos de la clase EscrituraClass
     * @throws SQLException Excepcion
     */
    public ArrayList getArraySearchEscriturasByTipo(String tipoEscritura) throws SQLException {
        ArrayList<EscrituraClass> arrayEscriturasLista = new ArrayList<>();

        conn = DriverManager.getConnection("jdbc:mysql://localhost/notaria", "root", "");
        sentencia = (com.mysql.jdbc.Statement) conn.createStatement();
        if (tipoEscritura.equalsIgnoreCase("NULL")) {
            resul = sentencia.executeQuery("SELECT * FROM escrituras");
        }
        resul = sentencia.executeQuery("SELECT * FROM escrituras WHERE Tipo = '" + tipoEscritura + "'  ");
        while (resul.next()) {
            String codEscritura = resul.getString(2);
            String tipo = resul.getString(3);
            String nombreFichero = resul.getString(4);
            int numInterv = resul.getInt(5);
            escritura = new EscrituraClass(codEscritura, tipo, nombreFichero, numInterv);
            arrayEscriturasLista.add(escritura);
        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement        
        return arrayEscriturasLista;
    }
}
