/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class ClienteClass {

    private String codigo;
    private String nombre;
    private int telefono;

    public ClienteClass() {
    }

    
    /**
     * Constructor exclusivo para codigo cliente
     * @param codigo String
     */
    public ClienteClass(String codigo) {
        this.codigo = codigo;
    }    
    
    /**
     * Constructor completo de un cliente
     * @param codigo String
     * @param nombre String
     * @param telefono Integer
     */
    public ClienteClass(String codigo, String nombre, int telefono) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
