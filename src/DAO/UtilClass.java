/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public final class UtilClass {

    BbClass bbdd;
    ClienteClass cliente;
    EscrituraClass escritura;

    static ArrayList<String> TipologiaEscriturasArrayList = new ArrayList<>();

    public UtilClass() {
        cargarArrayTipoEscrituras();
    }

    /**
     * Metodo que cargara los datos de las tipologias de escrituras
     */
    public void cargarArrayTipoEscrituras() {

        TipologiaEscriturasArrayList.add(0, "NULL");
        TipologiaEscriturasArrayList.add(1, "TEST");
        TipologiaEscriturasArrayList.add(2, "CPVE");
        TipologiaEscriturasArrayList.add(3, "PDR");
        TipologiaEscriturasArrayList.add(4, "DECHER");
        TipologiaEscriturasArrayList.add(5, "CAPMAT");
        TipologiaEscriturasArrayList.add(6, "PRESHIP");
        TipologiaEscriturasArrayList.add(7, "ACT");
        TipologiaEscriturasArrayList.add(8, "CONSOC");
        TipologiaEscriturasArrayList.add(9, "POL");
        TipologiaEscriturasArrayList.add(10, "PRO");

        //0 Selecciona un tipo .......
        //1 Testamento
        //2 Compraventa
        //3 Poder
        //4 Declaraci�n de herederos
        //5 Capitulaciones matrimoniales
        //6 Pr�stamo Hipotecario
        //7 Acta
        //8 Constituci�n de Sociedades
        //9 P�liza
        //10 Protesto
    }

    /**
     * Metodo que devolvera las siglas correspondientes al tipo de escritura
     * seleccionado
     *
     * @param posicion Integer
     * @return String
     */
    public String getTipoEscritura(int posicion) {
        return TipologiaEscriturasArrayList.get(posicion);
    }

    /**
     * Metodo que realiza la creacion de un nuevo cliente
     *
     * @param codigoCliente String - codigo unico de cliente
     * @param nombreCliente String - nombre del cliente
     * @param telefonoCliente Integer - Numero de telefono del cliente
     * @throws SQLException Excepcion
     */
    public void addNewCliente(String codigoCliente, String nombreCliente, int telefonoCliente) throws SQLException {
        bbdd = new BbClass();
        cliente = new ClienteClass(codigoCliente, nombreCliente, telefonoCliente);
        bbdd.insertCliente(cliente);
    }

    /**
     * Metodo que devolvera un objeto de la clase ClienteClass
     *
     * @param codigo String - codigo de cliente
     * @return Object de la clase ClienteClass
     * @throws SQLException Excepcion
     */
    public ClienteClass getCliente(String codigo) throws SQLException {
        bbdd = new BbClass();
        if (isExistCliente(codigo)) {
            return bbdd.getObjectCliente(codigo);
        }
        return null;
    }

    /**
     * Metodo para saber la existencia de un cliente por su codigo
     *
     * @param codigo String - codigo de clientye a buscar
     * @return Boolean
     * @throws SQLException Excepcion
     */
    private Boolean isExistCliente(String codigo) throws SQLException {
        bbdd = new BbClass();
        cliente = new ClienteClass(codigo);
        return bbdd.isCliente(cliente);
    }

    /**
     * Metodo con el que se pueden actualizar los datos de un cliente
     *
     * @param codCliente String - Codigo de cliente
     * @param nombre String - Nombre de cliente
     * @param telefono Integer - Numero de cliente
     * @throws SQLException Excepcion
     */
    public void actualizarCliente(String codCliente, String nombre, int telefono) throws SQLException {
        bbdd = new BbClass();
        cliente = new ClienteClass(codCliente, nombre, telefono);
        bbdd.updateCliente(cliente);
    }

    /**
     * Metodo que realiza la creacion de una nueva Escritura
     *
     * @param codigoEscritura String
     * @param posicionTipoEsc Integer
     * @param nombreFicheroEscritura String
     * @param numeroIntervinientes Integer
     * @throws java.sql.SQLException Escepcion
     */
    public void addNewEscritura(String codigoEscritura, int posicionTipoEsc, String nombreFicheroEscritura, int numeroIntervinientes) throws SQLException {
        bbdd = new BbClass();
        String tipoEscritura = getTipoEscritura(posicionTipoEsc);
        escritura = new EscrituraClass(codigoEscritura, tipoEscritura, nombreFicheroEscritura, numeroIntervinientes);
        bbdd.insertEscritura(escritura);
    }

    /**
     * Metodo que devolvera un objeto de la clase EscrituraClass
     *
     * @param codigo String - codigo de escritura
     * @return Object de la clase EscrituraClass
     * @throws SQLException Excepcion
     */
    public EscrituraClass getEscritura(String codigo) throws SQLException {
        bbdd = new BbClass();
        System.out.println(isExistEscritura(codigo));
        if (isExistEscritura(codigo)) {
            return bbdd.getObjectEscritura(codigo);
        }
        return null;
    }

    /**
     * Metodo para saber la existencia de una escritura por su codigo
     *
     * @param codigo String - codigo de la escritura a buscar
     * @return Boolean
     * @throws SQLException Excepcion
     */
    private Boolean isExistEscritura(String codigo) throws SQLException {
        bbdd = new BbClass();
        escritura = new EscrituraClass(codigo);
        return bbdd.isEscritura(escritura);
    }
    

    /**
     * Metodo que realiza la creacion de una nueva referencia en EscCli
     *
     * @param codCliente String - Codigo del cleinte
     * @param codEscritura String - Codigo de la escritura
     * @throws java.sql.SQLException Escepcion
     */
    public void addNewEscCli(String codCliente, String codEscritura) throws SQLException {
        bbdd = new BbClass();

        if (isExistCliente(codCliente) && isExistEscritura(codEscritura)) {
            bbdd.insertEscCli(codCliente, codEscritura);
        }
 
    }

    /**
     * Metodo que devolvera un el string del codigo de cliente vinculado al codigo de escritura
     *
     * @param codEscritura String - Codigo de la escritura
     * @return String - Codigo de cliente
     * @throws SQLException Excepcion
     */
    public String getCodClienteEscCli(String codEscritura) throws SQLException {
        bbdd = new BbClass();        
        return bbdd.getEscliCodigoCliente(codEscritura);        
    }
    
}
